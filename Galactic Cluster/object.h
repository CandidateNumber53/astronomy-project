#ifndef OBJECT_H
#define OBJECT_H

#include <armadillo>

using namespace arma;

class Object
{
public:
  double mass;
  vec pos, vel;
  Object ();
  Object (double, vec, vec);
};

#endif
