from numpy import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#plotting histogram of remaining particles
A = []
filename = open('rk4.txt')

for line in filename:
      A.append(asarray(map(float,line.strip().split())))
A = asarray(A)
B = A[-1]
rad = []
for i in range(0,len(B),3):
      r = sqrt(B[i]**2 + B[i+1]**2 + B[i+2]**2)
      rad.append(r)
rad = asarray(rad)
time = A[0][0]
rad = rad

n0 = 50
r0 = 0.5
r = linspace(0,max(rad),100)
n = n0/(1+(r/r0)**4)
plt.plot(r,n,label='theoretical solution')

plt.hist(rad,15)
plt.title(r'100 particles, $n_0=50, r_0=0.5$')
plt.xlabel(r'$r_0 [20ly]$')
plt.ylabel('Number of particles')
plt.legend()
plt.savefig('rad100.pdf',bbox_inches='tight')
plt.show()

#calculating and plotting virial theorem
virial = []
energy = []
n = 0
filename = open('energies.txt','r')
for line in filename:
      val = asarray(map(float,line.split()))
      energy.append(sum(val))
      virial1 = sum(val[::2])*2
      virial2 = -sum(val[1::2])
      virial.append(virial1/virial2)
      n += 1
virial = asarray(virial)
line = zeros(n) + 1
x = linspace(0,time,n)
plt.plot(x,virial,label='virial for our system')
plt.plot(x,line,label='virial for system in equilibrium')
plt.title(r'Virial Theorem, 100 objects')
plt.xlabel(r'$\tau_{crunch}$')
plt.ylabel(r'$(2<K>)/(-<V>)$')
plt.legend()
plt.savefig('virial100.pdf',bbox_inches='tight')
plt.show()

#plotting total energy
energy = asarray(energy)
plt.plot(x,energy)
plt.title(r'Total energy, 100 objects')
plt.xlabel(r'$\tau_{crunch}$')
plt.ylabel(r'$M_{\odot}(20ly)^2/(\tau_{crunch})^2$')
plt.axis([0,time,-2000,-500])
plt.savefig('energytot100.pdf',bbox_inches='tight')
plt.show()

