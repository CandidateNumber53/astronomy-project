#include "system.h"
#include <iomanip>
#include <fstream>

using namespace std;
using namespace arma;

System::System(double timestep, double time)
{
  nb_objects = 0;
  dt = timestep;
  tau = time;
}

void System::addobject(double mass, vec pos, vec vel)
{
  //adding objects to system
  Object x = Object(mass, pos, vel);
  objectlist.push_back(x);
  nb_objects += 1;
}

void System::energies()
{
  //calculating energies for the particles
  kinetic_energy.set_size(n);
  potential_energy.set_size(n);
  potential_energy_full.set_size(n);
  kinetic_energy.zeros();
  potential_energy.zeros();
  potential_energy_full.zeros();

  for(int i=0;i<n;i++)
    {
      for(int j=i+1;j<n;j++)
	{
          double x = A(6*i) - A(6*j);
	  double y = A(6*i+1) - A(6*j+1);
	  double z = A(6*i+2) - A(6*j+2);
	  double r = sqrt(x*x + y*y + z*z);
	  double pot_energy = G*objectlist[i].mass*objectlist[j].mass/r;
	  potential_energy(i) -= pot_energy*0.5;
	  potential_energy(j) -= pot_energy*0.5;
	  potential_energy_full(i) -= pot_energy;
	  potential_energy_full(j) -= pot_energy;
	}
    }
  double energy_tot = 0;
  for(int i=0;i<n;i++)
    {
      double v_squared = A(6*i+3)*A(6*i+3) + A(6*i+4)*A(6*i+4) + A(6*i+5)*A(6*i+5);
      kinetic_energy(i) = 0.5*objectlist[i].mass*v_squared;
      energy_tot += potential_energy(i) + kinetic_energy(i);
    }
  cout << "t: " << t << "   " << "dt: " << dt << "   " << "energy tot: " << energy_tot <<  endl;
}

void System::removing_particles()
{
  //removing particles that are being ejected from the system
  for(i=0;i<n;i++)
    {
      double r = sqrt(A(6*i)*A(6*i) + A(6*i+1)*A(6*i+1) + A(6*i+2)*A(6*i+2));
      if(kinetic_energy(i) > -potential_energy_full(i) and r > 1)
	{
	  objectlist.erase(objectlist.begin() + i);
	  n -= 1;
	  vec temp = zeros(6*n);
	  int k = 0;
	  for(int j=0;j<n;j++)
	    {
	      if(j == i)
		  {
		    k = 1;
		  }
	      temp(6*j) = A(6*(j+k));
	      temp(6*j+1) = A(6*(j+k)+1);
	      temp(6*j+2) = A(6*(j+k)+2);
	      temp(6*j+3) = A(6*(j+k)+3);
	      temp(6*j+4) = A(6*(j+k)+4);
	      temp(6*j+5) = A(6*(j+k)+5);
	    }
	  A.set_size(n);
	  A = temp;
	}
    }
}

void System::rk45()
{
  //calculating the next timestep and finding optimal timestep for next iteration
  vec k1(6*n), k2(6*n), k3(6*n), k4(6*n), k5(6*n), k6(6*n);
  k1 = forces(A)*dt; 
  k2 = forces(A + c1*k1)*dt; 
  k3 = forces(A + c2*k1 + c3*k2)*dt; 
  k4 = forces(A + c4*k1 - c5*k2 + c6 * k3)*dt;
  k5 = forces(A + c7*k1 - c8*k2 + c9*k3 - c10*k4)*dt;  
  k6 = forces(A - c11*k1 + c12*k2 - c13*k3 + c14*k4 - c15*k5)*dt; 
  vec a = A + c16*k1 + c17*k3 + c18*k4 - c19*k5; 
  vec b = A + c20*k1 + c21*k3 + c22*k4 - c23*k5 + c24*k6;
  vec diff = abs(a-b);
  double s = pow(tol*0.5*dt/max(diff),0.25);
  if(dt < 1e-7)
    {
      A = a;
      dt = 1e-7;
    }
  else
    {
      A = a;
      dt *= s;
    }
}

vec System::forces(vec B)
{
  //calculating the right hand side of the differential equations
  vec forces = zeros(3*n);
  double x,y,z,r,f;

  for(int i=0;i<n;i++)
    {
      double fx = 0;
      double fy = 0;
      double fz = 0;
#pragma omp parallel for reduction(+:fx, fy,fz) shared(forces) private(x,y,z,r,f) num_threads(2)
      for(int j=i+1;j<n;j++)
	{
	  x = B(6*i) - B(6*j);
	  y = B(6*i+1) - B(6*j+1);
	  z = B(6*i+2) - B(6*j+2);
	  r = sqrt(x*x + y*y + z*z);
	  f = -G*objectlist[i].mass*objectlist[j].mass/(r*(r*r + 1e-3));
	  fx += f*x;
	  fy += f*y;
	  fz += f*z;
	  forces(3*j) += -f*x;
	  forces(3*j+1) += -f*y;
	  forces(3*j+2) += -f*z;
	}
    forces(3*i) += fx;
    forces(3*i+1) += fy;
    forces(3*i+2) += fz;
    }
    for(i=0;i<n;i++)
      {
	B(6*i) = B(6*i+3);
	B(6*i+1) = B(6*i+4);
	B(6*i+2) = B(6*i+5);
	B(6*i+3) = forces[3*i]/objectlist[i].mass;
	B(6*i+4) = forces[3*i+1]/objectlist[i].mass;
	B(6*i+5) = forces[3*i+2]/objectlist[i].mass;
      }
    return B;
}

void System::initialize()
{
  //creating array A which stores positions and velocities for all the objects
  n = nb_objects;
  A = zeros(6*n);
  for(i=0;i<n;i++)
{
  A(6*i) = objectlist[i].pos(0);
  A(6*i+1) = objectlist[i].pos(1);
  A(6*i+2) = objectlist[i].pos(2);
  A(6*i+3) = objectlist[i].vel(0);
  A(6*i+4) = objectlist[i].vel(1);
  A(6*i+5) = objectlist[i].vel(2);
 }
}

void System::solve()
{
  //calculating positions and energies and saving to file
  ofstream myfile;
  myfile.open("rk4.txt");
  myfile << tau << " " << endl;
  ofstream myfile2;
  myfile2.open("energies.txt");
  initialize();
  tol = 1e-8;
  t = 0;
  int t_temp = 0;
  int k = 0;
  while(t <= tau)
    {
      t += dt;
      rk45();
      if(t_temp == 100*k)
	//saving values for every 100 timestep
	{
	  energies();
	  for(i=0;i<n;i++)
	    {
	      myfile << setprecision(4) << A(6*i) << " " << A(6*i+1) << " " << A(6*i+2) << " ";
	      myfile2 << setprecision(4) << kinetic_energy(i) << " " << potential_energy(i) << " ";
	    }
	  removing_particles();
	  k += 1;
	  myfile << endl;
	  myfile2 << endl;
	  cout << "number of particles remaining: " << n << endl;
	}
      t_temp += 1;
    }
  removing_particles();
  cout << "number of particles remaining: " << n << endl;
  for(i=0;i<n;i++)
    {
  myfile << setprecision(4) << A(6*i) << " " << A(6*i+1) << " " << A(6*i+2) << " ";
  myfile2 << setprecision(4) << kinetic_energy(i) << " " << potential_energy(i) << " ";
    }
  myfile.close();
  myfile2.close();
}
