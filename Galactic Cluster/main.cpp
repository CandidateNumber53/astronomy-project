#define ARMA_NO_DEBUG
#include "object.h"
#include "system.h"
#include "gaussiandeviate.h"
#include <time.h>
using namespace std;
using namespace arma;

int main()
{
  double dt = 1e-5, tau = 4;
  System cluster(dt, tau);
  double mass;
  int R0 = 1;
  vec pos = zeros(3);
  vec vel = zeros(3);
  double u, v, w, r, theta, phi;
  default_random_engine l;
  long int k = -1;
  normal_distribution<double> distribution(10,1);
  for(int i=0;i<100;i++)
    {
      mass = distribution(l);
      u = ran2(&k);
      v = ran2(&k);
      w = ran2(&k);
      phi = 2*pi*w;
      r = R0*pow(u,1.0/3);
      theta = acos(1-2*v);
      pos(0) = r*sin(theta)*cos(phi);
      pos(1) = r*sin(theta)*sin(phi);
      pos(2) = r*cos(theta);
      cluster.addobject(mass, pos, vel);
    }
  clock_t start, finish;
  start = clock();
  cluster.solve();
  finish = clock();
  cout << "Time usage: " << double (finish-start)/CLOCKS_PER_SEC << "s" << endl;
}
