#program for plotting positions of planets in 3d

from numpy import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

for filename in ['verlet.txt','rk4.txt']:
   A = loadtxt(filename,skiprows=1)
   n = len(A[0,:])
   m = len(A[:,0])
   years, dt = fromfile(filename,count=2,sep=' ')

   fig = plt.figure()
   ax = fig.add_subplot(111, projection='3d')
   for i in range(0,n,3):
       ax.plot(A[:,i], A[:,i+1], A[:,i+2])
       ax.hold('on')
   plt.title('%s, %g years, timestep: %g' % (filename.split('.')[0],years,dt))
   plt.xlabel('Au')
   plt.ylabel('Au')
   plt.savefig('%s.pdf' % (filename.split('.')[0]), bbox_inches='tight')
plt.show()

