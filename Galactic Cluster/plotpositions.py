#program for plotting positions of system when no particles are removed

from numpy import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

filename = 'rk4.txt'
A = loadtxt(filename,skiprows=1)
n = len(A[0,:])
m = len(A[:,0])

k = m/4
l = 1
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for i in range(0,n,3):
    ax.plot(A[k:k+1,i], A[k:k+1,i+1], A[k:k+1,i+2], 'ob')
    ax.auto_scale_xyz([-l,l], [-l, l], [-l, l])
plt.title(r'System at $\tau_{crunch}=7$')
plt.savefig('time1.pdf', bbox_inches='tight')
plt.show()

