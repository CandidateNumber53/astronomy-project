#include "object.h"
#include "system.h"
#include <iomanip>
using namespace std;
using namespace arma;

int main()
{
  //Solarsystem

  //earth
  vec pos1(2), vel1(2);
  pos1 = {1,0}, vel1 = {0,2*pi};
  //sun
  vec pos2(2), vel2(2);
  double vel_sun = 2*pi*3e-6 + 2*pi/sqrt(5.2)*9.5e-4 + 2*pi/sqrt(0.39)*1.2e-7 + 2*pi/sqrt(0.72)*2.45e-6 + 2*pi/sqrt(1.52)*3.3e-7 + 2*pi/sqrt(9.54)*2.75e-4 + 2*pi/sqrt(19.19)*4.4e-5 + 2*pi/sqrt(30.06)*5.15e-5 + 2*pi/sqrt(39.53)*6.55e-9;
  pos2 = {0,0}, vel2 = {0,-vel_sun};
  //jupiter
  vec pos3(2), vel3(2);
  pos3 = {5.2,0}, vel3 = {0,2*pi/sqrt(5.2)};
  //mercury
  vec pos4(2), vel4(2);
  pos4 = {0.39,0}, vel4 = {0,2*pi/sqrt(0.39)};
  //venus
  vec pos5(2), vel5(2);
  pos5 = {0.72,0}, vel5 = {0,2*pi/sqrt(0.72)};
  //mars
  vec pos6(2), vel6(2);
  pos6 = {1.52,0}, vel6 = {0,2*pi/sqrt(1.52)};
  //saturn
  vec pos7(2), vel7(2);
  pos7 = {9.54,0}, vel7 = {0,2*pi/sqrt(9.54)};
  //uranus
  vec pos8(2), vel8(2);
  pos8 = {19.19,0}, vel8 = {0,2*pi/sqrt(19.19)};
  //neptun
  vec pos9(2), vel9(2);
  pos9 = {30.06,0}, vel9 = {0,2*pi/sqrt(30.06)};
  //pluto
  vec pos10(2), vel10(2);
  pos10 = {39.53,0}, vel10 = {0,2*pi/sqrt(39.53)};

  double dt = 0.001, years = 250;
  System solarsystem(dt);
  Object earth(3e-6, pos1, vel1);
  Object sun(1, pos2, vel2);
  Object jupiter(9.5e-4, pos3, vel3);
  Object mercury(1.2e-7, pos4, vel4);
  Object venus(2.45e-6, pos5, vel5);
  Object mars(3.3e-7, pos6, vel6);
  Object saturn(2.75e-4, pos7, vel7);
  Object uranus(4.4e-5, pos8, vel8);
  Object neptun(5.15e-5, pos9, vel9);
  Object pluto(6.55e-9, pos10, vel10);
  solarsystem.addobject(earth);
  solarsystem.addobject(sun);
  solarsystem.addobject(jupiter);
  solarsystem.addobject(mercury);
  solarsystem.addobject(venus);
  solarsystem.addobject(mars);
  solarsystem.addobject(saturn);
  solarsystem.addobject(uranus);
  solarsystem.addobject(neptun);
  solarsystem.addobject(pluto);

  //calculating positions and saving to file
  ofstream myfile;
  myfile.open("RK4.txt");
  myfile << years << " " << dt << " " << endl;
  ofstream myfile2;
  myfile2.open("Verlet.txt");
  myfile2 << years << " " << dt << " " << endl;
  solarsystem.initialize_rk4();
  solarsystem.initialize_verlet();
  double t = 0;
  while(t <= years)
    {
      vec A = solarsystem.rk4();
      vec pos = solarsystem.verlet();
      t += dt;
      for(int i=0;i<10;i++)
	{
	  myfile << setprecision(4) << A(4*i) << " " << A(4*i+1) << " ";
	  myfile2 << setprecision(4) << " " << pos(2*i) << " " << pos(2*i+1) << " ";
	}
      myfile << endl;
      myfile2 << endl;
    }
  myfile.close();
  myfile2.close();
}
