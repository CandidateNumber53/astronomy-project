#program for plotting positions of planets

from numpy import *
import matplotlib.pyplot as plt

distance = []
for filename in ['Verlet.txt','RK4.txt']:

   A = loadtxt(filename,skiprows=1)
   n = len(A[0,:])
   m = len(A[:,0])
   years, dt = fromfile(filename,count=2,sep=' ')

   plt.figure()
   for i in range(0,n,2):
       plt.plot(A[:,i],A[:,i+1])
       plt.hold('on')
   plt.title('%s, %g years, timestep: %g' % (filename.split('.')[0],years,dt))
   plt.xlabel('Position [AU]')
   plt.ylabel('Position [AU]')
   plt.savefig('%s.pdf' % (filename.split('.')[0]), bbox_inches='tight')

plt.show()
